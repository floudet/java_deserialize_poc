# Java_deserialize_poc

Just a small PoC in Java to test/understand java deserialization bugs. It contains a "victim" program, and 2 others to create inputs, one "malicious" and one non malicious.
When deserialization bugs came out, particularly in java apps, I wanted to have a small PoC to properly understand the exploitation. It may help people visualize how those bugs are exploited in a very simple setup.

# Content

- `Serialize_string.java`:  just serialize a string, what the victim program is expecting.
- `AttackerClass.java`: serialize a malicious input, based on an "interesting" class that the attacker would know is already loaded by the victim.
- `Serial_victim`: the victim code, will take as input a serialized object, treat it as a string and try to just print its content.

# Compiling
` javac *.java`

# Executing
- Create the 2 inputs for the victim: ` java Serialize_string` then `java AttackerClass`
- Pass input of your choice to victim program `java Serial_victim what_is_expected.ser` or `java Serial_victim attacker_input.ser`

# Explanation

The main explanation is within AttackerClass.java. It shows that an attacker can serialize an object that would reference a Class loaded by the victim client. This class would contain a custom ReadObject method that would have interesting pieces of codes that the attacker would like to execute (in this case, on OSX, spawning calculator)