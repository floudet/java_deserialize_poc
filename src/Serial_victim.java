import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.io.IOException;

public class Serial_victim{
    public static void main(String args[]) throws Exception{

	String inputFile=args[0]; // parameter passed, either the expected input (a string) or the "malicious" one 
        FileInputStream fis = new FileInputStream(inputFile); 
        ObjectInputStream ois = new ObjectInputStream(fis);
        String StringFromDisk = (String)ois.readObject(); // <-- DESERIALIZING HERE
 
        //Print the result.
        System.out.println(StringFromDisk);
        ois.close();
	}
}
