import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.io.IOException;


public class Serialize_string{
    public static void main(String args[]) throws Exception{

// just serializing a string, to be used as input for "victim" program as "good" input
        String blah = "all is fine!";
        FileOutputStream fos = new FileOutputStream("what_is_expected.ser");
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(blah);
        os.close();
        }
}
